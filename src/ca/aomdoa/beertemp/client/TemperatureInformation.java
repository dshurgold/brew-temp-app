package ca.aomdoa.beertemp.client;

import java.util.ArrayList;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

/**
 * Initalized from the json resonse using gson
 * @author david
 *
 */
public class TemperatureInformation {
    private String last_sample;
    private Temperatures last, tenmin;
    private ArrayList<Temperatures> houravg;
    
    public static class Temperatures {
        private double sensor_one, sensor_two, board, air;
        
        public double getSensorOne() {
            return sensor_one;
        }
        
        public double getSensorTwo() {
            return sensor_two;
        }
        
        public double getBoard() {
            return board;
        }
        
        public double getAir() {
            return air;
        }
    }
    
    public DateTime getLastSampleDate() {
        String pattern = "YYYY-MM-dd HH:mm:ss";
        return DateTime.parse(last_sample, DateTimeFormat.forPattern(pattern).withZoneUTC());
    }
    
    public Temperatures getLast() {
        return last;
    }
    
    public Temperatures getTenMinutes() {
        return tenmin;
    }
    
    public List<Temperatures> getHourlyAverage() {
        return houravg;
    }
}
