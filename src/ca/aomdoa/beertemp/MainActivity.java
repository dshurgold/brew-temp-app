package ca.aomdoa.beertemp;

import java.util.Calendar;

import org.joda.time.DateTimeZone;

import ca.aomdoa.beertemp.client.TemperatureInformation;
import ca.aomdoa.beertemp.client.TemperatureInformation.Temperatures;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class MainActivity extends Activity {
    private static String URL = "http://aomdoa.ca:81/status";
    private RequestQueue requestQueue;
    private Listener<String> responseListener;
    private ErrorListener errorListener;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        requestQueue = Volley.newRequestQueue(this);
        responseListener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                TemperatureInformation ti = new Gson().fromJson(response, TemperatureInformation.class);
                Calendar cal = Calendar.getInstance();
                TextView txtLatestSample = (TextView)findViewById(R.id.txtLastLogTime);
                txtLatestSample.setText(ti.getLastSampleDate().toDateTime(DateTimeZone.forTimeZone(cal.getTimeZone())).toString("yyyy-MM-dd HH:mm:ss"));
                
                setTemperatures(ti.getLast(), R.id.txtLastSensorOne, R.id.txtLastSensorTwo, R.id.txtLastSensorRoom, R.id.txtLastSensorAir);
                setTemperatures(ti.getTenMinutes(), R.id.txtTenSensorOne, R.id.txtTenSensorTwo, R.id.txtTenSensorRoom, R.id.txtTenSensorAir);
                
                TableLayout table = (TableLayout)findViewById(R.id.tblLayoutHour);
                table.removeAllViews();
                for(Temperatures t : ti.getHourlyAverage()) {
                    TableRow row = new TableRow(MainActivity.this);
                    addTemperatureView(row, t.getSensorOne());
                    addTemperatureView(row, t.getSensorTwo());
                    addTemperatureView(row, t.getBoard());
                    addTemperatureView(row, t.getAir());
                    table.addView(row);
                }
            }
            
            public void setTemperatures(Temperatures temp, int sensorOne, int sensorTwo, int sensorBoard, int sensorAir) {
                TextView txtSensorOne = (TextView)findViewById(sensorOne);
                TextView txtSensorTwo = (TextView)findViewById(sensorTwo);
                TextView txtSensorBoard = (TextView)findViewById(sensorBoard);
                TextView txtSensorAir = (TextView)findViewById(sensorAir);
                txtSensorOne.setText(String.format("%.2f °C", temp.getSensorOne()));
                txtSensorTwo.setText(String.format("%.2f °C", temp.getSensorTwo()));
                txtSensorBoard.setText(String.format("%.2f °C", temp.getBoard()));
                txtSensorAir.setText(String.format("%.2f °C", temp.getAir()));
            }
            
            public void addTemperatureView(TableRow row, double temperature) {
                TextView tv = new TextView(MainActivity.this);
                tv.setText(String.format("%.2f °C", temperature));
                tv.setLayoutParams(new TableRow.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, 1f));
                row.addView(tv);
            }
        };
        
        errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                TextView txtLatestSample = (TextView)findViewById(R.id.txtLastLogTime);
                txtLatestSample.setText(error.toString());
            }
        };
        
        refresh(null);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    
    public void refresh(View view) {
        StringRequest jsObjRequest = new StringRequest(Request.Method.GET, URL, responseListener, errorListener);
        jsObjRequest.setShouldCache(false);
        requestQueue.add(jsObjRequest);
    }
}
